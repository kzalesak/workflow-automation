REM Batch file for automatic commit and push to bitbucket
@echo on
git add *
SET /p message=What do you commit?:
git commit -m "%message%"
git status

:choice
SET /p c=Do you want to push? P=yes:
@echo off
IF /I "%c%" NEQ "P" GOTO :no_push
IF /I "%c%" EQU "P" GOTO :do_push
@echo on
GOTO :choice

:do_push
git push

:no_push
PAUSE
EXIT