@echo off
ECHO Initialise user, repo and branch for git.

:user_init_choice
	SET /p c=Do you want to init a git user? Y=yes:
	IF /I "%c%" NEQ "Y" GOTO :rep_init_choice
	IF /I "%c%" EQU "Y" GOTO :user_init
GOTO :user_init_choice

:user_init
SET /p uname=What is your username:
SET /p umail=What is your email:

git init
git config user.name %uname%
git config user.email %umail%
echo.

:rep_init_choice
	SET /p c2=Do you want to checkout a repository? Y=yes:
	IF /I "%c2%" NEQ "Y" GOTO :branch_choice
	IF /I "%c2%" EQU "Y" GOTO :rep_init
GOTO :rep_init_choice

:rep_init
SET /p urep=URL of your repository (pulls master):
git init
git remote add origin %urep%
git pull origin master
git push --set-upstream origin master
echo.

:branch_choice
	SET /p c=Do you want to create a new branch? Y=yes:
	IF /I "%c%" NEQ "Y" GOTO :end
	IF /I "%c%" EQU "Y" GOTO :branch
GOTO :branch_choice


:branch
SET /p branch_name=What is your branch?:
git checkout -b "%branch_name%"
git push --set-upstream origin "%branch_name%"
echo.

:end
echo.
echo ------------------ SUMMARY ------------------
echo.
git status
echo.
PAUSE
ECHO Goodbye!
:end_exit
EXIT